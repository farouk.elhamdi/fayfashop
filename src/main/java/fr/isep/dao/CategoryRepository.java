package fr.isep.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.isep.entites.Category;



@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{
}
