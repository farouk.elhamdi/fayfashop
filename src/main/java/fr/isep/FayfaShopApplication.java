package fr.isep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FayfaShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(FayfaShopApplication.class, args);
	}

}
