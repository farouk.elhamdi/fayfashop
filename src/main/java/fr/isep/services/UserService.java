package fr.isep.services;

import java.util.List;

import fr.isep.entites.User;

public interface UserService {
	public User findByEmail(String email);
	
	public void save(User user);
	
	public void update(User user);
	
	public List<User> findAllUser();
	
	public void deleteUser(long userId);
}
